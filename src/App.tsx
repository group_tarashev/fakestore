import { useState } from "react";
import "./App.css";
import Nav from "./components/Nav";
import { CartProvider } from "./context/CartProvider";
import { ProductProvider } from "./context/ProductProvider";
import Cart from "./components/Cart";
import Product from "./components/Product";
import Header from "./components/Header";
import Footer from "./components/Footer";
import ProductsList from "./components/ProductsList";

function App() {
  const [viewCart, setViewCart] = useState<boolean>(false);

  const pageContent = viewCart ? <Cart/> : <ProductsList/>

  const content = (
    <>
      <Header viewCart = {viewCart} setViewCart= {setViewCart}/>
      {pageContent}
      <Footer viewCart = {viewCart}/>
    </>
  )
  
  return (
    <>
      <ProductProvider>
        <CartProvider>
          {content}
        </CartProvider>
      </ProductProvider>
    </>
  );
}

export default App;
