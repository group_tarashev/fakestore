import { useContext } from "react"
import CartContext, { UseCartContext } from "../context/CartProvider"
import { UseProductContextType } from "../context/ProductProvider";


const useCart = () : UseCartContext =>{
    return useContext(CartContext);
}

export default useCart;