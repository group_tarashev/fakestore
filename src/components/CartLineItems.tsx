import React, { ChangeEvent, ReducerAction } from "react";
import { CartType, ReducerActionType } from "../context/CartProvider";
import "../styles/cart.css";
import { AiOutlinePlus } from "react-icons/ai";
import { BsTrash } from "react-icons/bs";
import { AiOutlineMinusCircle } from "react-icons/ai";
type CartListType = {
  item: CartType;
  image: string;
  title: string;
  qty: number;
  price: number;
  dispatch: React.Dispatch<ReducerAction>;
  REDUCER_ACTIONS: ReducerActionType;
};

const CartLineItems = ({
  image,
  title,
  qty,
  dispatch,
  price,
  item,
  REDUCER_ACTIONS,
}: CartListType) => {
  const lineTotal: number = qty * price;
  const onChangeQty = () => {
    dispatch({
      type: REDUCER_ACTIONS.INCREASE,
      payload: { ...item, qty: qty + 1 },
    });
  };
  const onChangeDecr = () => {
    
    dispatch({
      type: REDUCER_ACTIONS.DECREASE,
      payload: { ...item, qty: qty - 1 },
    });
  };
  const onRemoveItem = () => {
    dispatch({
      type: REDUCER_ACTIONS.REMOVE,
      payload: item,
    });
  };
  if(item.qty === 0 ){
   onRemoveItem(); 
  }
  
  const content = (
    <div className="cart-line">
      <img width={150} height={150} src={image} alt={title} />
      <div className="cart-text">
        <p className="cart-title">{title}</p>
        <p className="cart-desc">{item.description}</p>
      </div>
      <div className="buttons-group">
        <span className="cart-qty">Quantity: {qty}</span>
        <button onClick={onRemoveItem}>
          <BsTrash />
        </button>
        <button onClick={onChangeQty}>
          <AiOutlinePlus />
        </button>
        <button onClick={onChangeDecr} disabled={qty === 0}>
          <AiOutlineMinusCircle />
        </button>
      </div>
    </div>
  );
  return <div className="cart-line-items">{content}</div>;
};

export default CartLineItems;
