import React, { ReducerAction, useState } from "react";
import { ReducerActionType } from "../context/CartProvider";
import { ProductType } from "../context/ProductProvider";
import "../styles/products.css";
type ProdType = {
  prod: ProductType;
  dispatch: React.Dispatch<ReducerAction>;
  REDUCER_ACTIONS: ReducerActionType;
  inCart: boolean
};

const Product = ({ prod, dispatch, REDUCER_ACTIONS, inCart }: ProdType) => {
  const onAddtoCart = () => {
    dispatch({ type: REDUCER_ACTIONS.ADD, payload: { ...prod, qty: 1 } });
  };
  const itemInCart = inCart ? <p style={{color:'green'}}>Item in Cart</p> : null
  const content = (
    <div className="product">
      <img
        src={prod.image}
        alt={prod.title}
        width={150}
        height={150}
        className="image"
      />
      <h3 className="title">{prod.title}</h3>
      <div className="content">
        <p>{prod.category}</p>
      </div>
            {itemInCart}
      <div className="price-order">
        <p>
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
          }).format(prod.price)}
        </p>
        <button className="btn-order" onClick={onAddtoCart}>Add to Cart</button>
      </div>
    </div>
  );
  return <div className="product-cards">{content}</div>;
};

export default Product;
