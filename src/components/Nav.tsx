import React from 'react'
import useProduct from '../hook/useProducts';

type PropsNav ={
  viewCart: boolean,
  setViewCart: React.Dispatch<React.SetStateAction<boolean>>
}

const Nav = ({viewCart, setViewCart}: PropsNav) => {

  const button = viewCart ?
    <button onClick={() => setViewCart(false)}>View Products</button>
    : 
    <button onClick={() => setViewCart(true)}>View Cart</button>
  const content = (
    <>
      {button}
    </>
  )
  return (

    <div className='nav'>
      {content}
    </div>
  )
}

export default Nav