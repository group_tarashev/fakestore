import React, { SetStateAction } from "react";
import Nav from "./Nav";
import useCart from "../hook/useCart";
import "../styles/header.css";
import { AiOutlineShoppingCart } from "react-icons/ai";
type PropsHeader = {
  viewCart: boolean;
  setViewCart: React.Dispatch<SetStateAction<boolean>>;
};

const Header = ({ viewCart, setViewCart }: PropsHeader) => {
  const { totalItem, cart  } = useCart();
  const open = () => {
    setViewCart(!viewCart);
  };
  
  const content = (
    <header className="header">
      <h3>FAKESTORE</h3>
      <button className="header-price-box" onClick={open} disabled={cart.length === 0}>
        {cart.length !== 0 && (<p> {totalItem} </p>)}
        <AiOutlineShoppingCart size={20} color={"white"} />
      </button>
      {viewCart ? <button onClick={open}  className="btn-store">Back to store</button> : null}
    </header>
  );

  return <div className="head">{content}</div>;
};

export default Header;
