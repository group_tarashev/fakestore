import React, { ReactElement, useState } from 'react'
import useProduct from '../hook/useProducts'
import useCart from '../hook/useCart'
import Product from './Product';
import { ProductType } from '../context/ProductProvider';
import '../styles/products.css'

const cat :Array<string>= ["All", "men's clothing",'jewelery','electronics',"women's clothing"];

const ProductsList = () => {
    const {dispatch, REDUCER_ACTIONS, cart} = useCart();
    const {products } = useProduct();
    const [catProd, setCatProd] = useState<string>('all')
    
    const pickCat = (e: any) =>{
        const str : string = e.target.innerText
        setCatProd(str.toLowerCase());
        
    }
    const product = (prod:ProductType, inCart: boolean) =>{
        return( <Product
        prod ={prod}
        key={prod.id}
        dispatch= {dispatch}
        REDUCER_ACTIONS ={REDUCER_ACTIONS}
        inCart={inCart}
        />)
    }
    let pageContent :ReactElement | ReactElement[] = <p>Loading ...</p>
    if(products?.length){
        pageContent = products?.map((prod) =>{
            const inCart = cart?.some((item) => item.id === prod.id)
            if(catProd === prod.category){
                return(
                    product(prod, inCart)
                    )
            }else if(catProd === 'all'){
                return(
                    product(prod, inCart)
                )
            }
            else{
            return <>
            </>
            }
}        )
    }
    const list = (
        <div className='main-category'>
            <h3>Category</h3>
        <ul className='category-list'>
            {cat.map((item, i) =>(
                <li onClick={(e) => pickCat(e)} key={i}>{item}</li>
            ))}
        </ul>
        </div>
    )
    const content = (
        <main className='main-product'>
            {pageContent}
        </main>
    )
    return (
        <div className='main'>
        {list}
        <h3>Products</h3>
        {content}
    </div>
  )
}

export default ProductsList