import React from "react";
import useCart from "../hook/useCart";
import '../styles/footer.css'
type PropsFooter = {
  viewCart: boolean;
};

const Footer = ({ viewCart }: PropsFooter) => {
  const { totalItem, totalPrice } = useCart();
  const fullYear = new Date().getFullYear();
  const content = viewCart || (
    <div className="footer">

      <p>Total Item: {totalItem}</p>
      <p>Total Price: {new Intl.NumberFormat('en-US',{style: "currency", currency: "USD"}).format(totalPrice) }</p>
    </div>
  );
  return <div>{content}</div>;
};

export default Footer;
