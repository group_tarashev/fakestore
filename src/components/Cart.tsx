import React, { useState } from "react";
import useCart from "../hook/useCart";
import CartLineItems from "./CartLineItems";
import '../styles/cart.css'
const Cart = () => {
  const [conf, setConf] = useState<boolean>(false);
  const { dispatch, REDUCER_ACTIONS, totalItem, totalPrice, cart } = useCart();

  const onSubmit = () => {
    setConf(true);
    setTimeout(() =>{
      dispatch({ type: REDUCER_ACTIONS.SUBMIT });
    },1000)
  };
  
  const pageContent = conf ? (
    <h3>Thanks for you Order</h3>
  ) : (
    <>
      <h2>Cart</h2>
      <div className="orders">

      <ul className="cart">
        {cart?.map((item) => {
          return (
            <CartLineItems
              key={item.id}
              item={item}
              image={item.image}
              title={item.title}
              price={item.price}
              qty={item.qty}
              dispatch={dispatch}
              REDUCER_ACTIONS= {REDUCER_ACTIONS}
            />
          );
        })}
      </ul>
      <div className="order">
        <p>Totla Item: {totalItem}</p>
        <p>Total Price: {new Intl.NumberFormat('en-US',{style: "currency", currency: "USD"}).format(totalPrice) }</p>
        <button className="btn-order" disabled={!totalItem} onClick={onSubmit} >Order</button>
      </div>
      </div>
    </>
  );

  return <div className="cart-items">{pageContent}</div>;
};

export default Cart;
