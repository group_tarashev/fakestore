import axios from "axios";
import { Children, ReactElement, createContext, useEffect, useState } from "react";

export type ProductType = {
    id: number,
    category: string,
    rating: {rate: number, count: number}
    title: string,
    price : number,
    image: string,
    description: string 
}

export const  initStateProduct : ProductType[] = [];

export type UseProductContextType = {products: ProductType[]};

const initContextState : UseProductContextType ={products: []};

const ProductContext = createContext<UseProductContextType>(initContextState);

type ChildrenType = {
    children: ReactElement | ReactElement[]
}

export const ProductProvider = ({children} : ChildrenType) : ReactElement =>{
    const [products, setProducts] = useState<ProductType[]>([])

    useEffect(() =>{
        const getAxios = async () =>{
            await axios.get("store.json")
                .then((res) =>{
                    setProducts(res.data)
                })
        }
        getAxios();
    },[])
    return (<ProductContext.Provider value={{products}}>{children}</ProductContext.Provider>)
}
export default ProductContext