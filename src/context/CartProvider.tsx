import { ReactElement, createContext, useMemo, useReducer } from "react";

export type CartType ={
    id: number,
    qty: number,
    price: number,
    image: string
    title: string,
    description: string,
    category: string,

}

export type CartStateType = {cart: CartType[]};

const initCartState: CartStateType = {cart: []}

const REDUCER_ACTION_TYPE ={
    ADD: "ADD",
    SUBMIT: "SUBMIT",
    QUANTITY: "QUANTITY",
    REMOVE : "REMOVE",
    INCREASE: "INCREASE",
    DECREASE: "DECREASE",
    CATEGORY: "CATEGORY"
}
export type ReducerActionType = typeof REDUCER_ACTION_TYPE;

export type ReducerAction ={
    type: string,
    payload?: CartType, 
}

const reducer = (state: CartStateType, action: ReducerAction) : CartStateType => {
    switch(action.type){
        case REDUCER_ACTION_TYPE.ADD :{
            if(!action.payload) throw new Error("Missing in action")
            const {id, title, price, image, description, category} = action?.payload

            const filt: CartType[] = state.cart.filter(item => item.id !== id)

            const itemExist : CartType | undefined = state.cart.find(item => item.id === id);

            const qty: number = itemExist ? itemExist.qty + 1: 1;
            
            return {...state ,cart: [...filt, {id, qty, price, image, title, description, category}]}
        } 
        case REDUCER_ACTION_TYPE.REMOVE :{
            if(!action.payload) {throw new Error("Missing in action")}
            const {id} = action.payload;
            const filt: CartType[] = state.cart.filter(item => item.id !== id)
            return {...state, cart: [...filt]}
            
        }
        case REDUCER_ACTION_TYPE.INCREASE :{
            if(!action.payload) {throw new Error("Missing in action")}
            const {id, qty} = action.payload;
            console.log(action.payload);
            
            const filt: CartType[] = state.cart.filter(item => item.id !== id)
            const isExist : CartType | undefined = state.cart.find(item => item.id === id)
            if(!isExist) throw new Error("Item does not exist") 
            const update :CartType = {...isExist, qty}
            return {...state, cart:[...filt, update]}
            
        }
        
       
        case REDUCER_ACTION_TYPE.DECREASE :{
            if(!action.payload){ throw new Error("Missing in action")}
            const {id, qty} = action.payload;
            
            const isExist: CartType | undefined = state.cart.find(item => item.id === id);
            
            if(!isExist){ throw new Error("Item does not exist")}
            const updatedQty : CartType = {...isExist, qty}
            const filt : CartType[] = state.cart.filter(item => item.id !== id)

            return {...state, cart: [...filt, updatedQty]}
            
        }

        case REDUCER_ACTION_TYPE.SUBMIT :{
            return {...state, cart: []}
        }
        default: 
            throw new Error("Undefined action type")
    }
}

const  useCartContext = (initCartState : CartStateType) =>{
    const [state, dispatch] = useReducer(reducer, initCartState)

    const REDUCER_ACTIONS = useMemo(() =>{
        return REDUCER_ACTION_TYPE
    },[])

    const totalItem : number  = state.cart.reduce((prev, cartItem) =>{
        return prev + cartItem.qty
    },0)

    const totalPrice : number = state.cart.reduce((prev, cartItem) =>{
        return prev + (cartItem.price * cartItem.qty)
    },0)
    const cart = state.cart.sort((a,b) =>{
        const it1 = Number(a.id);
        const it2 = Number(b.id);

        return it1 - it2;
        
    });
    return {dispatch, totalItem, totalPrice, cart, REDUCER_ACTIONS}
}

export type UseCartContext = ReturnType< typeof useCartContext>;

const initCartContextState: UseCartContext= {
    dispatch: () => {},
    REDUCER_ACTIONS: REDUCER_ACTION_TYPE,
    totalItem: 0,
    totalPrice: 0,
    cart: []
}

const CartContext = createContext<UseCartContext>(initCartContextState) ;

type ChildrenType = { children : ReactElement | ReactElement[]}

export const CartProvider = ({children} : ChildrenType) =>{
    return <CartContext.Provider value={useCartContext(initCartState)}>{children}</CartContext.Provider>
}

export default CartContext